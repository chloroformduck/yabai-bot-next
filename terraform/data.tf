data "template_file" "instance_startup" {
  template = file("${path.module}/data/instance-startup.sh")

  vars = {
    token       = var.token
    environment = var.environment
    guild       = var.guild
    branch      = var.branch
    volume      = "/dev/sdh" # this has to be hard coded because it causes a cycle otherwise
  }
}

data "template_cloudinit_config" "instance_userdata" {
  gzip          = false
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    filename     = "10-instance-userdata"
    content      = "#!/bin/sh\necho -n '${base64gzip(data.template_file.instance_startup.rendered)}' | base64 -d | gunzip | /bin/sh"
  }
}

data "aws_ami" "al2023" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["al2023-ami-2023.*-arm64"]
  }
}
