resource "aws_instance" "bot" {
  ami                         = data.aws_ami.al2023.id
  instance_type               = "t4g.nano"
  subnet_id                   = var.subnet_id
  user_data_base64            = base64encode(data.template_cloudinit_config.instance_userdata.rendered)
  key_name                    = "key2023"
  associate_public_ip_address = true
  security_groups = [ "sg-0696e3129318a68f9" ]
  tags = {
    Name = "yabai-bot-${var.environment}"
  }
}

resource "aws_ebs_volume" "bot_storage" {
  availability_zone = "us-west-2a"
  size              = 16
}

resource "aws_volume_attachment" "bot_volume" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.bot_storage.id
  instance_id = aws_instance.bot.id
}
