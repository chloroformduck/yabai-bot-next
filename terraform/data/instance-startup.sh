#!/bin/sh

export token="${token}"
export environment="${environment}"
export guild="${guild}"
export db_path="/data/data.yml"
export log_file_name="/data/bot.log"

dnf install -y git-all gcc python3.11

if ! [ -f $db_path ]; then mkfs -t xfs ${volume}; mkdir /data; mount ${volume} /data; chmod a+rw /data; fi

mkdir ./application
cd ./application
git clone https://gitlab.com/chloroformduck/yabai-bot-next.git
cd yabai-bot-next
git checkout "${branch}"
python3.11 -m venv .env
. ./.env/bin/activate
pip install -r ./src/python/requirements.txt
python ./src/python/main.py &