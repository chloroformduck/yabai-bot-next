variable "image" {
  description = "The docker image to use on the droplet"
}

variable "token" {
  description = "discord bot token"
  sensitive   = true
}

variable "docker_registry" {
  description = "the registry to pull the docker image from"
  default     = "registry.gitlab.com"
}

variable "environment" {
  description = "the environment the bot is running in"
  default     = "dev"
}

variable "guild" {
  description = "the guild commands will be run in, used for testing the bot"
  default     = ""
}

variable "subnet_id" {
  description = "The subnet to launch the bot instance in"
  type        = string
}

variable "key_name" {
  description = "The ssh key name to use in the instance"
  default     = ""
}

variable "branch" {
  description = "The branch to deploy to the server"
  default     = "main"
}
