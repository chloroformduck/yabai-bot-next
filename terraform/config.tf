terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  backend "s3" {
    key    = "next.tfstate"
    bucket = "terraform-state-320015742285"
    region = "us-west-2"
  }
}

provider "aws" {
  region = "us-west-2"
}
