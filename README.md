# Yabai Bot Next

2.0 version of my discord bot  

Current Features:  
* Entirely managed via slash commands and interactions - no chat commands to remember  
* User role selection  
* Anti-bot entry prompt management   
* Welcome message  
* Random fun things  
  
Planned Features:  
* Context menu reporting - right click a message to notify moderators  
* Message edit logging  
* More as I think of them