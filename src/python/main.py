import logging
from config import ConfigReader
from interactions import Client, Intents, MISSING, listen

conf = ConfigReader('config.yml')
environ = conf.get('environment')
token = conf.get('token')
guild = conf.get('guild')
log_file_name = conf.get('log_file_name')
if not guild:
    guild = MISSING

if environ == 'prod':
    loglevel = "warning"
else:
    loglevel = "info"
numeric_level = getattr(logging, loglevel.upper(), None)
logging.basicConfig(format='%(asctime)s|%(levelname)s|%(name)s:  %(message)s', filename=log_file_name, level=numeric_level, datefmt='%Y-%m-%d %H:%M:%S')

bot = Client(default_scope=guild, intents=Intents.DEFAULT | Intents.GUILD_MEMBERS | Intents.GUILD_MESSAGE_TYPING)

modules = ["manage", "user", "fun", "listeners"]

for module in modules:
    bot.load_extension(module)

bot.start(token)