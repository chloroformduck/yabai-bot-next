import logging
import random
from db import Botdb
from interactions import Extension, Permissions, SlashContext, SlashCommandOption,\
     OptionType, slash_command

from config import ConfigReader

conf = ConfigReader('config.yml')

database = conf.get('db_path')

db = Botdb(database)

logging.getLogger('Manage')

class Fun(Extension):
    "Extension for fun stuff"

    @slash_command(
        name="ffxiv",
        description="Post a very important message into the chat",
        default_member_permissions=Permissions.SEND_MESSAGES
    )
    async def _ffxiv(self, ctx: SlashContext):
        await ctx.send("Did you know that the critically acclaimed MMORPG Final Fantasy XIV has a free trial, and includes the entirety of A Realm Reborn AND the award-winning Heavensward expansion up to level 60 with no restrictions on playtime? Sign up, and enjoy Eorzea today! https://secure.square-enix.com/account/app/svc/ffxivregister?lng=en-us")

    @slash_command(
        name="whitelist",
        description="Request that your username get whitelisted",
        default_member_permissions=Permissions.SEND_MESSAGES,
        options=[
            SlashCommandOption(
            type=OptionType.STRING,
            name="username",
            description="The username you want to be whitelisted",
            required=True,
            )
        ]
    )
    async def _whitelist(self, ctx: SlashContext, username: str):
        mod_channels = [await ctx.guild.fetch_channel(channel) for channel in db.get_channels_for_action("mod", ctx.guild_id)]
        if mod_channels:
            for channel in mod_channels:
                await channel.send(f"User {ctx.member.display_name} is requesting whitelisting in <#{ctx.channel_id}>\nTheir username is: {username}")
            await ctx.send("Your request has been submitted to the moderation team", ephemeral=True)
        else:
            await ctx.send("The moderation team of this server has not set up a moderator's channel. If you believe that they should have one set up, please report this error to them", ephemeral=True)


    @slash_command(
        name="dice_roll",
        description="Roll some dice",
        options=[
            SlashCommandOption(
                type=OptionType.INTEGER,
                name="number",
                description="The number of dice to throw",
                required=True
            ),
            SlashCommandOption(
                type=OptionType.INTEGER,
                name="sides",
                description="The number of dice faces",
                required=True
            )
        ]
    )
    async def _dice_roll(self, ctx: SlashContext, number: int, sides: int):
        roll = [random.randint(1, sides) for _ in range(number)]
        await ctx.send(f"<@{ctx.member.id}> rolled {number}d{sides} and got {sum(roll)}\n({', '.join(str(i) for i in roll)})")