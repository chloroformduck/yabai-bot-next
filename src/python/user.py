import logging
from db import Botdb
from interactions import Permissions, Role, ChannelType, InputText, TextStyles, Modal, Snowflake,\
        SlashContext, Extension, StringSelectMenu, RoleSelectMenu, ChannelSelectMenu, Member,\
        StringSelectOption, ActionRow, ComponentContext, Button, ButtonStyle, ModalContext,\
        slash_command, component_callback
from interactions.api.events import Component

from config import ConfigReader

logging.getLogger('User')

conf = ConfigReader('config.yml')

database = conf.get('db_path')

db = Botdb(database)

class User(Extension):
    "Extension for managing users"

    def compare_user(self, user1: Snowflake, user2: Snowflake):
        return user1 == user2

    @slash_command(
        name="role_menu_setup",
        description="Starts a modal for creating a role select menu.",
        default_member_permissions=Permissions.MANAGE_ROLES
    )
    async def _role_menu_setup(self, ctx: SlashContext):
        role_menu = [ActionRow(RoleSelectMenu(placeholder="Selectable roles", custom_id="role_selection", max_values=25)),
                    ActionRow(ChannelSelectMenu(channel_types=ChannelType.GUILD_TEXT, placeholder="Post channel", custom_id="role_channel"))]
        confirm_button = [ActionRow(Button(
            label="Confirm",
            custom_id="post_button",
            style=ButtonStyle.SUCCESS
        ),
            Button(
                label="Cancel",
                custom_id="cancel_button",
                style=ButtonStyle.DANGER
        ))]
        question = InputText(
            style=TextStyles.SHORT,
            custom_id="role_menu_message",
            label="Message",
            required=False
        )
        modal = Modal(
            question,
            title="Role Menu Message (Optional)",
        )
        user_id = ctx.user.id
        await ctx.send_modal(modal)
        modal_ctx: ModalContext = await ctx.bot.wait_for_modal(modal)
        role_message = modal_ctx.responses["role_menu_message"]
        edit_message = await modal_ctx.send("Use the following to set up the roles and the channel to post the selection into", components=role_menu)
        roles = []
        role_channel = None
        try:
            selected_role: Component = await self.bot.wait_for_component(components=[ActionRow(RoleSelectMenu(placeholder="Selectable roles", custom_id="role_selection", max_values=25))], timeout=120)
        except TimeoutError:
            await edit_message.edit(content="Interaction timed out. Please use /role_setup_menu to start again.", components=[])
            return
        else:
            if self.compare_user(user_id, selected_role.ctx.author.id):
                role_comp = selected_role.ctx
                roles: list[Role] = role_comp.values
                await role_comp.send(f"Roles set to {', '.join([role.name for role in roles])}", ephemeral=True)
            else:
                await selected_role.ctx.send("This isn't meant for you", ephemeral=True)
        try:
            selected_channel: Component = await self.bot.wait_for_component(components=[ActionRow(ChannelSelectMenu(channel_types=ChannelType.GUILD_TEXT, placeholder="Post channel", custom_id="role_channel"))], timeout=120)
        except TimeoutError:
            await edit_message.edit(content="Interaction timed out. Please use /role_setup_menu to start again.", components=[])
            return
        else:
            if self.compare_user(user_id, selected_channel.ctx.author.id):
                channel_comp = selected_channel.ctx
                role_channel = await self.bot.fetch_channel(channel_comp.values[0])
                edit_message = await edit_message.edit(content=f"Use the following to set up the role selection\nChannel selected: {role_channel.name}>", components=[ActionRow(RoleSelectMenu(placeholder="Selectable roles", custom_id="role_selection", max_values=25))])
            else:
                await selected_channel.ctx.send("This isn't meant for you", ephemeral=True)
        option_list = [
            StringSelectOption(label=role.name, value=str(role.id)) for role in roles
        ]
        edit_message = await edit_message.edit(content=f"This will post a message to {role_channel.name} that allows anyone to select these roles:\n{', '.join([role.name for role in roles])}\nTo confirm, press the button below within 2 minutes", components=confirm_button)
        try:
            button: Component = await self.bot.wait_for_component(components=confirm_button, timeout=120)
        except TimeoutError:
            await edit_message.edit(content="Interaction timed out. Please use /role_setup_menu to start again.", components=[])
            return
        else:
            if self.compare_user(user_id, selected_channel.ctx.author.id):
                if button.ctx.custom_id == "post_button":
                    await role_channel.send(role_message, components=StringSelectMenu(option_list, placeholder="Select a role", custom_id="role_menu_selection"))
                    await edit_message.delete()
                    await button.ctx.send("Successfully set up role menu", ephemeral=True)
                else:
                    await edit_message.delete()
                    await button.ctx.send("Cancelled role menu setup", ephemeral=True)
            else:
                await selected_channel.ctx.send("This isn't meant for you", ephemeral=True)

    @component_callback("role_menu_selection")
    async def role_response(self, ctx: ComponentContext):
        added = []
        removed = []
        for role in ctx.values:
            guild_role: Role = await ctx.guild.fetch_role(int(role))
            if int(guild_role.id) not in ctx.member.roles:
                await ctx.member.add_role(guild_role, "Menu Selection")
                added.append(guild_role.name)
            else:
                await ctx.member.remove_role(guild_role, "Menu Selection")
                removed.append(guild_role.name)
        if len(added) > 0:
            await ctx.send(f"Added {', '.join(added)} to your roles", ephemeral=True)
        elif len(removed) > 0:
            await ctx.send(f"Removed {', '.join(removed)} from your roles", ephemeral=True)
        else:
            await ctx.send("No change was made to your roles", ephemeral=True)