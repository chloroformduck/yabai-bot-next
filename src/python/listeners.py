import logging
from db import Botdb
from interactions import Extension, Member, Guild, Embed, EmbedAttachment, MaterialColors, listen
from interactions.api.events import MemberAdd
from config import ConfigReader

logging.getLogger('Listeners')

conf = ConfigReader('config.yml')

database = conf.get('db_path')

db = Botdb(database)

class Listeners(Extension):
    "Extension for listeners"

    def _generate_user_embed(self, user: Member, guild: Guild):
        guild_name = guild.name
        user_avatar = user.avatar.as_url()
        banner_message = f"Thank you for joining {guild_name}!"
        body_message = f"Welcome, <@{user.id}>. Please check out the rules channel, and enjoy your stay!"
        return Embed(
            title=banner_message,
            thumbnail=EmbedAttachment(
                url=user_avatar
            ),
            color=MaterialColors.INDIGO,
            description=body_message
        )

    @listen(MemberAdd)
    async def _member_join_actions(self, event: MemberAdd):
        if db.get_channels_for_action('welcome', int(event.guild_id)):
            logging.info(f"{event.member.global_name} joined {event.guild_id}")
            welcome_channels = [await event.guild.fetch_channel(channel) for channel in db.get_channels_for_action('welcome', int(event.guild_id))]
            user_embed = self._generate_user_embed(event.member, event.guild)
            for welcome_channel in welcome_channels:
                await welcome_channel.send(embeds=user_embed)