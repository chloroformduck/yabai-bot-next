import logging
from db import Botdb
from interactions import Permissions, Role, InputText, TextStyles,\
        SlashContext, OptionType, ActionRow, Button, ButtonStyle, Modal,\
        Extension, GuildChannel, SlashCommandOption, ComponentContext, \
        slash_command, component_callback, modal_callback

from config import ConfigReader

logging.getLogger('Manage')

conf = ConfigReader('config.yml')

database = conf.get('db_path')

db = Botdb(database)

class Manage(Extension):
    "Extension for guild management"

    @slash_command(
        name="toggle_log_channel",
        description="Set or unset a channel to receive bot logs",
        options=[
            SlashCommandOption(
            type=OptionType.CHANNEL,
            name="channel",
            description="The channel to toggle",
            required=True,
            )
        ],
        default_member_permissions=Permissions.ADMINISTRATOR
    )
    async def _set_log_channel(self, ctx: SlashContext, channel: GuildChannel):
        logging.info(f"Toggling channel {channel.name} as a logging channel for guild {ctx.guild_id}")
        if db.toggle_channel_action(int(channel.id), "log", int(ctx.guild_id)):
            await ctx.send(f"Successfully set {channel.name} as a logging channel. All bot logging output will go there", ephemeral=True)
        else:
            await ctx.send(f"Removed {channel.name} as a logging channel. It will no longer receive logging output", ephemeral=True)

    @slash_command(
        name="set_mod_channel",
        description="Use this command to set or unset your moderator channel. This should be a mod only channel",
        options=[
            SlashCommandOption(
            type=OptionType.CHANNEL,
            name="channel",
            description="The channel to toggle",
            required=True,
            )
        ],
        default_member_permissions=Permissions.ADMINISTRATOR
    )
    async def _set_mod_channel(self, ctx: SlashContext, channel: GuildChannel):
        logging.info(f"Toggling channel {channel.name} as a moderation channel for guild {ctx.guild_id}")
        if db.toggle_channel_action(int(channel.id), "mod", int(ctx.guild_id)):
            await ctx.send(f"Successfully set {channel.name} as a moderator channel. All mod related output will go there", ephemeral=True)
        else:
            await ctx.send(f"Removed {channel.name} as a mod channel. It will no longer receive mod output", ephemeral=True)

    @slash_command(
        name="toggle_welcome_channel",
        description="Use this command to set or remove a channel as a welcome channel.",
        options=[
            SlashCommandOption(
            type=OptionType.CHANNEL,
            name="channel",
            description="The channel to toggle",
            required=True,
            )
        ],
        default_member_permissions=Permissions.ADMINISTRATOR
    )
    async def _set_welcome_channel(self, ctx: SlashContext, channel: GuildChannel):
        logging.info(f"Toggling channel {channel.name} as a welcome channel for guild {ctx.guild_id}")
        if db.toggle_channel_action(int(channel.id), "welcome", int(ctx.guild_id)):
            await ctx.send(f"Successfully set {channel.name} as a welcome channel. New users will be welcomed there", ephemeral=True)
        else:
            await ctx.send(f"Removed {channel.name} as a welcome channel. It will no longer receive welcome output", ephemeral=True)

    @slash_command(
        name="entry_prompt",
        description="Sets up the entry prompt. Creating a new one will break any existing prompt",
        options=[
            SlashCommandOption(
            type=OptionType.CHANNEL,
            name="channel",
            description="The channel to post to",
            required=True,
            ),
            SlashCommandOption(
                type=OptionType.STRING,
                name="prompt",
                description="The question to ask the user",
                required=True
            ),
            SlashCommandOption(
                type=OptionType.STRING,
                name="answer",
                description="The answer to the question",
                required=True
            ),
            SlashCommandOption(
                type=OptionType.ROLE,
                name="role",
                description="The role to give to the user if they answer correctly",
                required=True
            )
        ],
        default_member_permissions=Permissions.ADMINISTRATOR
    )
    async def _config_entry_prompt(self, ctx: SlashContext, prompt: str, answer: any, role: Role, channel: GuildChannel):
        row = ActionRow(Button(
            label="Entry Prompt",
            custom_id="entry_button",
            style=ButtonStyle.PRIMARY
        ))
        await ctx.send("Configuring entry prompt", ephemeral=True)
        post_channel = await self.bot.fetch_channel(channel.id)
        await post_channel.send(f"To gain access to this server, click the button and type out the answer to this question:\n> {prompt}", components=row)
        db.set_member_role(int(role.id), int(ctx.guild_id))
        db.put_prompt_answer(int(ctx.guild_id), answer)

    @component_callback("entry_button")
    async def _entry_button(self, ctx: ComponentContext):
        question = InputText(
            style=TextStyles.SHORT,
            custom_id="entry_prompt_response",
            label="Answer"
        )
        modal = Modal(
            question,
            title="Entry Prompt",
            custom_id="entry_prompt",
        )
        await ctx.send_modal(modal)

    @modal_callback("entry_prompt")
    async def _prompt_reponse(self, ctx: ComponentContext, entry_prompt_response):
        log_channels = db.get_channels_for_action("log", ctx.guild_id)
        logging.info(f"{ctx.member.global_name} gave answer {entry_prompt_response} to {ctx.guild_id} and {db.get_prompt_answer(int(ctx.guild_id))} was expected")
        if db.get_prompt_answer(int(ctx.guild_id)) == entry_prompt_response:
            await ctx.member.add_role(db.get_member_role(int(ctx.guild_id)), "Correctly answered the entry prompt")
            await ctx.send("You answered the prompt correctly, welcome to the server!", ephemeral=True)
            if log_channels:
                for log_channel in log_channels:
                    send_channel = await self.bot.fetch_channel(log_channel)
                    await send_channel.send(f"User {ctx.member.global_name} ({ctx.member.id}) answered the entry prompt correctly and was allowed access to the server")
        else:
            await ctx.send("You did not answer the prompt correctly. Try again, or contact the moderators", ephemeral=True)
            if log_channels:
                for log_channel in log_channels:
                    send_channel = await self.bot.get_channel(log_channel)
                    await send_channel.send(f"User {ctx.member.global_name} ({ctx.member.id}) answered the entry prompt incorrectly and was not allowed access to the server")