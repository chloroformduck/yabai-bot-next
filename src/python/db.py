import logging
import yaml
from pathlib import Path
from pprint import pprint

logging.getLogger('DB')

class Botdb():
    "Class to handle database functions in my discord bots"
    def __init__(self, db_file) -> None:
        self.db_file = Path(db_file)
        if self.db_file.is_file():
            with open(self.db_file, 'r') as f:
                self.data_stream = {doc['type']: doc['data'] for doc in yaml.safe_load_all(f)}
                print("Loaded Datastream")
                pprint(self.data_stream)
        else:
            self.data_stream = {}

    def _save(self) -> None:
        "Saves the currently loaded data stream back to the file"
        data = [{"type": k, "data": v} for k,v in self.data_stream.items()]
        with open(self.db_file, "w") as f:
            yaml.safe_dump_all(data, f)

    def _iterate_keys(self, keys: list) -> None:
        "Adds a series of nested dictionaries to the data_stream"
        selected = self.data_stream
        for k in keys:
            if not selected.get(k, None):
                selected[k] = {}
            selected = selected[k]

    def _delete_key(self, keys: list) -> None:
        "Takes a list of keys and deletes the last key"
        selected = self.data_stream
        for k in keys[:-1]:
            selected = selected[k]
        selected.pop(keys[-1])

    # def check_guild_option(self, guild_id: int, option: str) -> bool:
    #     "Checks if an option is enabled for a guild"
    #     self._iterate_keys(['options'])
    #     info: list = self.data_stream['options'].get(option, [])
    #     return guild_id in info

    # def toggle_guild_option(self, guild_id: int, option:str) -> None:
    #     "Toggles an option for a guild"
    #     self._iterate_keys(['options', option])
    #     if self.check_guild_option(guild_id, option):
    #         self.data_stream['options'][option].remove(guild_id)
    #     else:
    #         self.data_stream['options'][option].append(guild_id)
    #     self._save()

    def toggle_channel_action(self, channel: int, action: str, guild_id: int):
        "Sets or removes a channel for an action. Returns False if the channel is removed, returns True if the channel is added"
        self._iterate_keys(['channels', action, guild_id])
        if channel in self.get_channels_for_action(action, guild_id):
            self.data_stream['channels'][action][guild_id].remove(channel)
            self._save()
            return False
        else:
            if self.data_stream['channels'][action][guild_id] == {}:
                self.data_stream['channels'][action][guild_id] = [channel]
            else:
                self.data_stream['channels'][action][guild_id].append(channel)
            self._save()
            return True

    def get_channels_for_action(self, action: str, guild_id: int):
        "Gets the channels that an action is assigned to"
        self._iterate_keys(['channels', action])
        return self.data_stream['channels'][action].get(guild_id, None)
    
    def put_prompt_answer(self, guild_id: int, answer: str):
        "Associates a prompt answer to a guild id"
        self._iterate_keys(['prompts', guild_id])
        self.data_stream['prompts'][guild_id] = answer
        self._save()

    def get_prompt_answer(self, message_id: int):
        "Gets the prompt answer for the selected message"
        self._iterate_keys(['prompts'])
        return self.data_stream['prompts'].get(message_id, None)
    
    def delete_prompt_message(self, message_id: int):
        "Deletes the prompt from the database"
        self._delete_key(['prompts', message_id])
    
    def get_message_action(self, message_id: int):
        "Returns the action associated with a message"
        self._iterate_keys(['actions'])
        return self.data_stream['actions'].get(message_id, None)
    
    def toggle_message_action(self, message_id: int, action: str):
        "Sets or unsets an action for a message"
        self._iterate_keys(['actions', message_id])
        if action in self.get_message_action(message_id):
            self.data_stream['actions'][message_id].remove(action)
        else:
            if self.data_stream['actions'][message_id] == {}:
                self.data_stream['actions'][message_id] = [action]
            else:
                self.data_stream['actions'][message_id].append(action)
        self._save()

    def set_member_role(self, role: int, guild_id: int):
        "Sets the member role for a guild"
        self._iterate_keys(['roles', 'member', guild_id])
        self.data_stream['roles']['member'][guild_id] = role

    def get_member_role(self, guild_id: int):
        "Returns the member role id for a guild"
        self._iterate_keys(['roles', 'member', guild_id])
        return self.data_stream['roles']['member'].get(guild_id, None)
