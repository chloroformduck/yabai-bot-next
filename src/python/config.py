import logging
import os
import yaml

logging.getLogger()

class ConfigReader():
    """Helper class for reading config options from environment variables or the config file.
    Prioritizes looking for an environment variable, and if not found will fall back to the config file if present.

    Attributes:
        file_path -- an optional path to a config file in yml format
    """    
    def __init__(self, file_path='config.yml') -> None:
        try:
            with open(file_path, 'r') as doc:
                self.config = yaml.safe_load(doc)
        except FileNotFoundError as e:
            logging.info("Config file not found")
            self.config = {}

    def get(self, key: str) -> str:
        """Read a config key and returns the value or None if the key isn't found

        :param key: Key to look for
        :type key: str
        :return: Value of the key or None
        :rtype: str
        """  
        var = os.getenv(key)      
        if var:
            return var
        else:
            try:
                return self.config[key]
            except AttributeError:
                logging.warn("No environment variable found with the specified key and config file not found")
                return None
            except KeyError:
                logging.warn(f"{key} was not found in the loaded config file")
                return None